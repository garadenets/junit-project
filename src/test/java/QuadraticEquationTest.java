import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class QuadraticEquationTest {

    QuadraticEquation quadraticEquation = new QuadraticEquation();


    @Test
    public void quationTestNoRoots() {
        List<Double> expectedResult = new ArrayList<Double>();
        List<Double> result = quadraticEquation.roots(4, 2, 5);
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void quationTestTwoRoots() {
        List<Double> expectedResult = new ArrayList<Double>(Arrays.asList(-1.0, -2.0));
        List<Double> result = quadraticEquation.roots(1, 3, 2);
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void quationTestOneRoot() {
        List<Double> expectedResult = new ArrayList<Double>(Arrays.asList(-1.0));
        List<Double> result = quadraticEquation.roots(1, 2, 1);
        Assert.assertEquals(expectedResult, result);
    }

    @Test
    public void quationTestInfinity() {
        List<Double> expectedResult = new ArrayList<Double>(Arrays.asList(Double.POSITIVE_INFINITY, Double.NEGATIVE_INFINITY));
        List<Double> result = quadraticEquation.roots(0, 0, 0);
        Assert.assertEquals(expectedResult, result);
    }

}
