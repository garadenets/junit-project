import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter coefficients a");
        double a = Double.parseDouble(scanner.nextLine());
        System.out.println("Enter coefficients b");
        double b = Double.parseDouble(scanner.nextLine());
        System.out.println("Enter coefficients c");
        double c = Double.parseDouble(scanner.nextLine());
        QuadraticEquation quadraticEquation = new QuadraticEquation();
        List<Double> result = quadraticEquation.roots(a, b, c);
        if (result.size() > 0) {
            for (double res : result) {
                System.out.println(res);
            }
        } else {
            System.out.println("Quadratic equation does't have roots");
        }
    }
}
