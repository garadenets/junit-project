import java.util.ArrayList;
import java.util.List;

public class QuadraticEquation {

    public List<Double> roots(double a, double b, double c) {
        List<Double> x = new ArrayList<>();
        if ((a == 0) && (b == 0) && (c == 0)) {
            x.add(Double.POSITIVE_INFINITY);
            x.add(Double.NEGATIVE_INFINITY);
            return x;
        }
        double d = b * b - 4 * a * c;
        if (d > 0) {
            x.add((-b + Math.sqrt(d)) / 2 * a);
            x.add((-b - Math.sqrt(d)) / 2 * a);
        } else if (d == 0) {
            x.add((-b) / 2 * a);
        }
        return x;
    }
}
